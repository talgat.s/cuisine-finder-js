const devConfig = {
	presets: ["@babel/typescript"],
	plugins: [
		"@babel/plugin-proposal-class-properties",
		"@babel/plugin-transform-modules-commonjs",
		[
			"babel-plugin-relative-path-import",
			{
				paths: [
					{
						rootPathPrefix: "@",
						rootPathSuffix: "src/"
					}
				]
			}
		]
	]
};

const prodConfig = {
	ignore: [
		"src/**/__tests__/**",
		"src/**/*.spec.ts",
		"src/**/*.test.ts",
		// Ignore config dir
		"src/config/**"
	],
	presets: ["@babel/typescript"],
	plugins: [
		"@babel/plugin-proposal-class-properties",
		"@babel/plugin-transform-modules-commonjs",
		"transform-remove-debugger",
		"transform-node-env-inline",
		[
			"babel-plugin-relative-path-import",
			{
				paths: [
					{
						rootPathPrefix: "@",
						rootPathSuffix: "build/"
					}
				]
			}
		],
		"minify-guarded-expressions",
		"minify-constant-folding",
		"minify-dead-code-elimination"
	]
};

module.exports = process.env.NODE_ENV === "production" ? prodConfig : devConfig;
