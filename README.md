# cuisine-finder-node

[![pipeline status](https://gitlab.com/talgat.s/cuisine-finder-js/badges/master/pipeline.svg)](https://gitlab.com/talgat.s/cuisine-finder-js/commits/master)

Server of cuisine-finder

## <a name="intstallation"></a>Installation

	$ npm install

## Local

### <a name="local-post-intstallation"></a>Post-Installation

1. You will need local postgres database
2. Env variables by ising [dotenv](https://github.com/motdotla/dotenv#readme) create `.env.local`, adding
```
	PORT=Number, default 4000
	AUTH_SECRET_KEY=Any secret work for token
	DATABASE_URL=Local Database URI, you can user remote one
	SSL_MODE=Boolean depending if you enabled ssl, default true
```
3. You can [configure Database](#local-configure-db)

### <a name="local-configure-db"></a>Configure Database

Technically you can use remote db just change in `.env.local` `DATABASE_URL`, but not suggested. You can also populate or drop schema(clear everything, "factory clean") on local db.

#### Populate

	$ npm run db:populate

#### Drop schema

	$ npm run db:drop-schema

### Start

After [installation](#intstallation), [post-installation](#local-post-intstallation) and [configuring db](#local-configure-db)

	$ npm start

## License

MIT
