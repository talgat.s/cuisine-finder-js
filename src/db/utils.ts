export type Input = number | string | boolean;
export type InputObj = { [key: string]: Input };
export type QueryObj = { text: string; values: Input[]; rowMode?: "array" };

export function qry(strings: TemplateStringsArray, ...data: any[]) {
	let text = "";
	const values: Input[] = [];

	for (let i = 0; i < data.length; i++) {
		const s = strings[i];
		const d = data[i];

		if (typeof d === "string") {
			text += `${s}${d}`;
		} else if (typeof d === "function") {
			const cb = (value: Input, type = "") => {
				const position = values.push(value);
				return `$${position}${type}`;
			};
			const placeholder = d(cb);
			text += `${s}${placeholder}`;
		}
	}
	text += strings[strings.length - 1];

	return {
		text,
		values
	};
}

qry.s = (value: string) => `'${value}'`;

export function insert(inputObj: InputObj) {
	const columns = [];
	const values = [];
	const text = [];

	const entries = Object.entries(inputObj);
	for (let i = 0; i < entries.length; i++) {
		const [col, val] = entries[i];
		columns.push(col);
		values.push(val);
		text.push(`$${i + 1}`);
	}

	return {
		columns: columns.join(", "),
		values,
		text: text.join(", ")
	};
}

export function update(inputObj: InputObj) {
	const text = [];
	const values = [];

	const entries = Object.entries(inputObj);
	for (let i = 0; i < entries.length; i++) {
		const [col, val] = entries[i];
		values.push(val);
		text.push(`${col} = $${i + 1}`);
	}

	return {
		values,
		text: text.join(", ")
	};
}
