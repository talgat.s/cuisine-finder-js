import DataBase from "./DataBase";
import * as query from "./utils";

class Table {
	constructor(
		public db: DataBase,
		public tableName: string,
		public COLUMNS: { [key: string]: string }
	) {}

	public create() {
		return this.db.query(`CREATE TABLE IF NOT EXISTS ${this.tableName};`);
	}

	public drop() {
		return this.db.query(`DROP TABLE IF EXISTS ${this.tableName};`);
	}

	public insert(inputs: query.InputObj, returning: string = "") {
		const { columns, text, values } = query.insert(inputs);

		const string = `
			INSERT INTO ${this.tableName} (${columns})
			VALUES (${text})
			${returning};
		`;
		return this.db.query({
			text: string,
			values
		});
	}

	public update(
		inputs: query.InputObj,
		clause: query.QueryObj,
		returning: string = ""
	) {
		const update = query.update(inputs);

		const text = `
			UPDATE ${this.tableName}
			SET ${update.text}
			${clause.text}
			${returning};
		`;
		return this.db.query({
			text,
			values: [...update.values, ...clause.values]
		});
	}

	public delete(clause: query.QueryObj, returning: string = "") {
		const text = `
			DELETE FROM ${this.tableName}
			${clause.text}
			${returning};
		`;

		return this.db.query({
			text,
			values: clause.values
		});
	}

	public getErrorMessage() {
		return "Something went wrong";
	}
}

export default Table;
