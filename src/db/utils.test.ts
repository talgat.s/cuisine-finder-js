import * as utils from "./utils";

describe("test db/utils", () => {
	enum COLUMNS {
		AGE = "age",
		NAME = "name",
		SURNAME = "surname",
		GROUP = "group",
		GRANDPARENT = "grandparent"
	}
	const data = {
		[COLUMNS.NAME]: "Harry",
		[COLUMNS.SURNAME]: "Potter",
		[COLUMNS.AGE]: 12
	};
	describe("insert", () => {
		const { columns, text, values } = utils.insert(data);
		test("should return correct columns", () => {
			const colArr = columns.split(", ");

			expect(colArr).toHaveLength(3);
			expect(colArr).toEqual(
				expect.arrayContaining([COLUMNS.NAME, COLUMNS.SURNAME, COLUMNS.AGE])
			);
		});

		test("should return correct text", () => {
			expect(text).toBe("$1, $2, $3");
		});

		test("should return correct values", () => {
			expect(values).toHaveLength(3);
			expect(values).toEqual(expect.arrayContaining(["Harry", "Potter", 12]));
		});
	});

	describe("update", () => {
		const { text, values } = utils.update(data);
		test("should return correct text", () => {
			// We do not know how text will be assign
			let arrText: any[] = [];
			for (const a of text.split(", ")) {
				arrText = [...arrText, ...a.split(" = ")];
			}

			expect(arrText).toHaveLength(6);
			expect(arrText).toEqual(
				expect.arrayContaining(["age", "$1", "name", "$2", "surname", "$3"])
			);
		});

		test("should return correct values", () => {
			expect(values).toHaveLength(3);
			expect(values).toEqual(expect.arrayContaining(["Harry", "Potter", 12]));
		});
	});

	describe("qry", () => {
		test("should return correct text & values", () => {
			// prettier-ignore
			const { text, values } = utils.qry`
				WHERE ${COLUMNS.GROUP} = ${(v: Function) => v("Hogwart")} OR ${COLUMNS.AGE} = ${(v: Function) => v(15)}
			`;

			expect(text.trim()).toBe(
				`WHERE ${COLUMNS.GROUP} = $1 OR ${COLUMNS.AGE} = $2`
			);
			expect(values).toHaveLength(2);
			expect(values).toEqual(["Hogwart", 15]);
		});

		test("should return empty values for no concat values", () => {
			const { text, values } = utils.qry`
				WHERE GROUP = "Hogwart" OR AGE = 15
			`;

			expect(text.trim()).toBe('WHERE GROUP = "Hogwart" OR AGE = 15');
			expect(values).toHaveLength(0);
		});
	});
});
