import pg from "pg";
import { qry, QueryObj } from "./utils";

type ConnectCb = (db: DB) => Promise<any>;
type QueryArg = string | QueryObj;

class DB {
	private _pool: pg.Pool;
	private _client: pg.PoolClient | null;

	public constructor() {
		this._pool = new pg.Pool({
			connectionString: process.env.DATABASE_URL,
			ssl: process.env.SSL_MODE !== "false"
		});
		this._client = null;
	}

	public async connect(cb: ConnectCb): Promise<void | Error> {
		try {
			this._client = await this._pool.connect();

			try {
				return await cb(this);
			} catch (err) {
				return Promise.reject(err);
			} finally {
				this._client.release();
				this._client = null;
			}
		} catch (err) {
			console.error("Connect DB ERROR");
			return Promise.reject(err);
		}
	}

	public query(arg: QueryArg) {
		return this._client ? this._client.query(arg) : this._pool.query(arg);
	}

	public qry = qry;
}

export default DB;
