import DataBase from "./DataBase";
export { default as Table } from "./Table";
export { qry } from "./utils";

export default DataBase;
