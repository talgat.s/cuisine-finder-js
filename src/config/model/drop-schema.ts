import chalk from "chalk";

import Model from "@/model/Model";

(async () => {
	const model = new Model();
	try {
		console.info(chalk.green.bold("🎬 Dropping schema ..."));
		await model.connect(async model => {
			try {
				await model.query(model.qry`
					DROP SCHEMA public CASCADE;
					CREATE SCHEMA public;

					GRANT ALL ON SCHEMA public TO postgres;
					GRANT ALL ON SCHEMA public TO public;

				`);
			} catch (err) {
				return Promise.reject(err);
			}
		});
		console.info(chalk.green.bold("🏁 Finished dropping db"));
	} catch (err) {
		console.error(chalk.red.bold("❌ Terminated dropping schems"));
		console.error(err);
	}
})();
