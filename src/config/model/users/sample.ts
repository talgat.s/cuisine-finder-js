import { COLUMNS } from "@/model/users";
import { ROLE } from "@/user/index";

const data = [
	{
		[COLUMNS.NAME]: "Leonard",
		[COLUMNS.SURNAME]: "Hofstadter",
		[COLUMNS.EMAIL]: "iambatman@comicon.com",
		[COLUMNS.ROLE]: ROLE.EDITOR
	},
	{
		[COLUMNS.NAME]: "Penny",
		[COLUMNS.SURNAME]: "Hofstadter",
		[COLUMNS.EMAIL]: "queen_penelope@hellokitty.com",
		[COLUMNS.ROLE]: ROLE.GUEST
	},
	{
		[COLUMNS.NAME]: "Sheldon",
		[COLUMNS.SURNAME]: "Cooper",
		[COLUMNS.EMAIL]: "livelongandprosper@enterprice.fed",
		[COLUMNS.ROLE]: ROLE.ADMIN
	},
	{
		[COLUMNS.NAME]: "Howard",
		[COLUMNS.SURNAME]: "Wolowitz",
		[COLUMNS.EMAIL]: "wolowizard@comicon.com",
		[COLUMNS.ROLE]: ROLE.GUEST
	},
	{
		[COLUMNS.NAME]: "Rajesh",
		[COLUMNS.SURNAME]: "Koothrappali",
		[COLUMNS.EMAIL]: "hateinidiancuisine@comicon.com",
		[COLUMNS.ROLE]: ROLE.GUEST
	}
];

export default data;
