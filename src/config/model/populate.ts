import chalk from "chalk";

import { Table } from "@/db/index";
import Model from "@/model/Model";
import users from "@/config/model/users";
import organizations from "@/config/model/organizations";

const model = new Model();

async function populateTable(table: Table, data: any[]) {
	console.info(
		chalk.blue(`Populating ${chalk.yellow.italic.bold(table.tableName)} table`)
	);
	const promises = [];
	for (const d of data) {
		promises.push(table.insert(d));
	}
	return Promise.all(promises);
}

async function populateTables(model: Model) {
	try {
		await populateTable(model.users, users);
		await populateTable(model.organizations, organizations);
	} catch (err) {
		return Promise.reject(err);
	}
}

function deleteDuplicateData(model: Model) {
	let promises = [];
	for (const user of users) {
		promises.push(
			model.users.delete(
				model.qry`WHERE email = ${(v: Function) => v(user.email)}`
			)
		);
	}

	for (const organization of organizations) {
		promises.push(
			model.organizations.delete(
				model.qry`WHERE
					reg_num = ${(v: Function) => v(organization.reg_num)} OR
					company_name = ${(v: Function) => v(organization.company_name)}`
			)
		);
	}

	return Promise.all(promises);
}

function createTables(model: Model) {
	return Promise.all([model.users.create(), model.organizations.create()]);
}

(async () => {
	try {
		console.info(chalk.green.bold("🎬 Starting populating db ..."));
		await model.connect(async () => {
			try {
				await createTables(model);
				await deleteDuplicateData(model);
				await populateTables(model);
			} catch (err) {
				return Promise.reject(err);
			}
		});
		console.info(chalk.green.bold("🏁 Finished populating db"));
	} catch (err) {
		console.error(chalk.red.bold("❌ Terminated populating"));
		console.error(err);
	}
})();
