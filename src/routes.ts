import Model from "@/model/index";
import Route from "koa-router";
import User, { ROLE } from "./user/";

const routes = (model: Model, router: Route, user: User) => {
	router
		.get("Get all organizations", "/organizations", async ctx => {
			try {
				const result = await model.query(
					model.qry`SELECT * FROM ${model.organizations.tableName}`
				);

				ctx.body = result.rows;
			} catch (error) {
				ctx.status = 404;
				ctx.body = { error };
			}
		})
		.get("Get all organizations", "/organizations/:id", async ctx => {
			try {
				const { COLUMNS } = model.organizations;
				const result = await model.query(
					model.qry`
						SELECT * FROM ${model.organizations.tableName}
						WHERE ${COLUMNS.REG_NUM} = ${(v: Function) => v(ctx.params.id)}
					`
				);

				ctx.body = result.rows[0];
			} catch (error) {
				ctx.status = 404;
				ctx.body = { error };
			}
		})
		.post(
			"Add restaurant",
			"/restaurants",
			user.is([ROLE.GUEST, ROLE.EDITOR, ROLE.ADMIN]),
			async ctx => {
				try {
					const result = await model.organizations.insert(
						ctx.request.body,
						"RETURNING *"
					);

					ctx.body = result.rows;
				} catch (error) {
					if (error.code === "23505") {
						ctx.status = 409;
					} else {
						ctx.status = 404;
					}

					ctx.body = { error };
				}
			}
		);
};

export default routes;
