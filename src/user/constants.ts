export const SECRET = process.env.AUTH_SECRET_KEY || "AUTH_SECRET_KEY";
export enum ROLE {
	GUEST = "GUEST",
	EDITOR = "EDITOR",
	ADMIN = "ADMIN"
}
