import util from "util";
import Koa from "koa";
import jwt from "jsonwebtoken";

import Role, * as TRole from "koa2-rbac";
import { SECRET, ROLE } from "./constants";

type TokenArgs = { id: string; name: string; role: string };

const verify = util.promisify<
	string,
	string | Buffer,
	jwt.VerifyOptions | undefined,
	TokenArgs
>(jwt.verify);
const getRole: TRole.GetRole = ctx => ctx._user.role;
const denyHandler: Koa.Middleware = ctx => {
	const { _user: user, _matchedRouteName: matchedRouteName } = ctx;
	ctx.status = 403;
	ctx.body = {
		user,
		error: matchedRouteName
			? `Access Denied - You don't have permission to :: ${matchedRouteName}`
			: "Access Denied - You don't have permission"
	};
};

class User extends Role {
	constructor() {
		super({ getRole, denyHandler });
	}

	async middleware(ctx: Koa.Context, next: Function): Promise<void> {
		const { authorization: token } = ctx.request.header;
		let user;
		if (token) {
			user = await verify(token, SECRET, undefined);
		}
		ctx._user = user || {
			id: "",
			name: "",
			role: ROLE.GUEST
		};

		await next();
	}

	token({ id, name, role }: TokenArgs): string {
		return jwt.sign({ id, name, role }, SECRET, { noTimestamp: true });
	}
}

export default User;
