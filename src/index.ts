import Koa from "koa";
import Route from "koa-router";
import cors from "@koa/cors";
import bodyParser from "koa-bodyparser";

import Model from "./model/";
import User from "./user/";
import routes from "./routes";

const app = new Koa();
const router = new Route();
const model = new Model();
const user = new User();

app.use(cors());
app.use(bodyParser());

app.use(user.middleware);

routes(model, router, user);
app.use(router.routes()).use(router.allowedMethods());

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
	console.warn(`App is running on \x1b[33m ${PORT} \x1b[0m`);
});
