import DB from "@/db/";

import Organizations from "./organizations/";
import Users from "./users/";

class Model extends DB {
	public organizations: Organizations;
	public users: Users;

	constructor() {
		super();

		this.organizations = new Organizations(this);
		this.users = new Users(this);
	}
}

export default Model;
