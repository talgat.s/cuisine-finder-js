export const NAME = "organizations";

export const COLUMNS = {
	REG_NUM: "reg_num",
	COMPANY_NAME: "company_name",
	START_DATE: "start_date",
	END_DATE: "end_date",
	BRAND_NAME: "brand_name",
	LOGO: "logo",
	ACTIVITY: "activity",
	DESCRIPTION: "description",
	ADDRESS: "address",
	EMAIL: "email",
	SITE: "site",
	TEL: "tel",
	INSPECTOR: "inspector",
	CREATED: "created",
	UPDATED: "updated"
};
