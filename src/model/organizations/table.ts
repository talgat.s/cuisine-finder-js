import DB, { Table } from "@/db/index";
import { NAME, COLUMNS } from "./constants";
import create from "./create";

class Organizations extends Table {
	constructor(db: DB) {
		super(db, NAME, COLUMNS);
	}

	public create = create;
}

export default Organizations;
