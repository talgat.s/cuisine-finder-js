import { Table, qry } from "@/db/index";

function create(this: Table) {
	const table = this.tableName;
	const {
		COMPANY_NAME,
		REG_NUM,
		START_DATE,
		END_DATE,
		BRAND_NAME,
		LOGO,
		ACTIVITY,
		DESCRIPTION,
		ADDRESS,
		EMAIL,
		SITE,
		TEL,
		INSPECTOR,
		CREATED,
		UPDATED
	} = this.COLUMNS;

	// prettier-ignore
	const query = qry`
		CREATE TABLE IF NOT EXISTS ${table}(
			${REG_NUM} text PRIMARY KEY UNIQUE,
			${COMPANY_NAME} text UNIQUE NOT NULL CHECK (char_length(${COMPANY_NAME}) >= 3),
			${START_DATE} timestamptz NOT NULL,
			${END_DATE} timestamptz NOT NULL,
			${BRAND_NAME} text NOT NULL,
			${LOGO} text,
			${ACTIVITY} text,
			${DESCRIPTION} text,
			${ADDRESS} text,
			${EMAIL} text,
			${SITE} text,
			${TEL} text,
			${INSPECTOR} text,
			${CREATED} timestamptz NOT NULL DEFAULT NOW(),
			${UPDATED} timestamptz NOT NULL DEFAULT NOW()
		);
	`;

	return this.db.query(query);
}

export default create;
