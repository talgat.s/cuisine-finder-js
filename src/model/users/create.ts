import { Table, qry } from "@/db/index";
import { ROLE as ROLE_ENUM } from "@/user/index";

const emailRegExp = /^[a-z0-9_%+.-]+@[a-z0-9.-]+\.[a-z]{2,4}$/;

function create(this: Table) {
	const table = this.tableName;
	const { ID, NAME, SURNAME, EMAIL, ROLE, CREATED, UPDATED } = this.COLUMNS;

	// prettier-ignore
	const createQuery = qry`
		CREATE EXTENSION IF NOT EXISTS "pgcrypto";

		DO $$
		BEGIN
			IF NOT EXISTS (SELECT 1 FROM pg_type WHERE typname = 'role_type') THEN
	        CREATE TYPE role_type AS ENUM (
						${Object.values(ROLE_ENUM)
							.map(role => qry.s(role))
							.join(", ")}
					);
	    END IF;
	  END$$;

		CREATE TABLE IF NOT EXISTS ${table} (
			${ID} text PRIMARY KEY UNIQUE DEFAULT CONCAT('user-', gen_random_uuid()),
			${NAME} text NOT NULL,
			${SURNAME} text NOT NULL,
			${EMAIL} text UNIQUE NOT NULL
				CHECK (${EMAIL} ~* ${qry.s(emailRegExp.source)}),
			${ROLE} role_type NOT NULL DEFAULT ${qry.s(ROLE_ENUM.GUEST)},
			${CREATED} timestamptz NOT NULL DEFAULT NOW(),
			${UPDATED} timestamptz NOT NULL DEFAULT NOW(),
			UNIQUE (${NAME}, ${SURNAME})
		);
	`;

	return this.db.query(createQuery);
}

export default create;
