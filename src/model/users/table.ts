import DB, { Table } from "@/db/index";
import { NAME, COLUMNS } from "./constants";
import create from "./create";

class Users extends Table {
	constructor(db: DB) {
		super(db, NAME, COLUMNS);
	}

	public create = create;
}

export default Users;
