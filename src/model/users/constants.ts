export const NAME = "users";

export const COLUMNS = {
	ID: "id",
	NAME: "name",
	SURNAME: "surname",
	EMAIL: "email",
	ROLE: "role",
	CREATED: "created",
	UPDATED: "updated"
};
